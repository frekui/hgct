# Copyright (c) 2005 Fredrik Kuivinen <frekui@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

from ctcore import *
import qt, re, os
qconnect = qt.QObject.connect
Qt = qt.Qt

class CommitDialog(qt.QDialog):
    def __init__(self, mergeMsg, commitMsg, files):
        qt.QDialog.__init__(self)
        self.setModal(True)
        self.setCaption('Confirm Commit - ' + applicationName)
        self.layout = qt.QVBoxLayout(self)
        self.layout.setAutoAdd(True)
        self.layout.setMargin(10)
        self.layout.setSpacing(10)

        self.msgL1 = qt.QLabel('<qt><p>' + mergeMsg + '</p>' + \
                               '<p>Do you want to commit the following file(s):</p></qt>', self)
        self.fileList = qt.QListBox(self)
        self.fileList.setSelectionMode(qt.QListBox.NoSelection)
        for f in files:
            self.fileList.insertItem(f)
        self.fileList.setMinimumSize(self.fileList.width(),
                                     self.fileList.itemHeight()*10)

        self.msgL2 = \
          qt.QLabel('<qt><p>with the commit message:</p>' + \
                    '<blockquote><pre>' + unicode(qt.QStyleSheet.escape(commitMsg)) + '</pre></blockquote></qt>',
                    self)

        self.buttonL = qt.QHBox(self)
        self.yes = qt.QPushButton("&Yes", self.buttonL)
        self.no = qt.QPushButton("&No", self.buttonL)
        qconnect(self.yes, qt.SIGNAL("clicked()"), self.accept)
        qconnect(self.no, qt.SIGNAL("clicked()"), self.reject)

        self.yes.setFocus()

    def paintEvent(self, e):
        qt.QDialog.paintEvent(self, e)
