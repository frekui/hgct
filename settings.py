# Copyright (c) 2005 Fredrik Kuivinen <frekui@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

from ctcore import *
import qt, re, os
qconnect = qt.QObject.connect
Qt = qt.Qt

class Settings(qt.QDialog):
    def __init__(self):
        qt.QDialog.__init__(self)
        self.setModal(True)
        self.setCaption('Preferences')
        self.layout = qt.QVBoxLayout(self)
        self.layout.setMargin(10)
        self.layout.setSpacing(10)

        self.gridL = qt.QGridLayout(2, 3)
        self.gridL.setColStretch(2, 1)
        self.layout.addLayout(self.gridL)
        
        self.quitBox = qt.QCheckBox(self)
        self.quitLabel = qt.QLabel('Exit on no changed files', self)
        self.quitLabel.setBuddy(self.quitBox)
        self.gridL.addWidget(self.quitBox, 0, 0)
        self.gridL.addWidget(self.quitLabel, 0, 1)

        self.exclBox = qt.QGroupBox(1, Qt.Horizontal, 'Ignore patterns (Git only)', self)
        self.layout.addWidget(self.exclBox)

        self.exclL = qt.QGrid(2, self.exclBox)
        self.exclFLabel = qt.QLabel('Load and save patterns from/to ', self.exclL)
        self.exclFE = qt.QLineEdit(self.exclL)
        self.exclFLabel.setBuddy(self.exclFE)

        self.exclDLabel = qt.QLabel('Load per directory patterns from ', self.exclL)
        self.exclDE = qt.QLineEdit(self.exclL)
        self.exclDLabel.setBuddy(self.exclDE)

        self.layout.addSpacing(10)
        
        self.signoffLabel = qt.QLabel('Commit signoff:', self)
        self.signoffBox = qt.QTextEdit(self)
        self.signoffLabel.setBuddy(self.signoffBox)
        self.layout.addWidget(self.signoffLabel)
        self.layout.addWidget(self.signoffBox)

        self.buttonL = qt.QHBox(self)
        self.layout.addWidget(self.buttonL)
        self.ok = qt.QPushButton("&Ok", self.buttonL)
        self.cancel = qt.QPushButton("&Cancel", self.buttonL)
        qconnect(self.ok, qt.SIGNAL("clicked()"), self.accept)
        qconnect(self.cancel, qt.SIGNAL("clicked()"), self.reject)

        self.resize(self.size())
        self.qset = qt.QSettings()
        self.loadSettings()
        
    def loadSettings(self):
        self.qset.setPath(shortName, shortName)
        self.qset.resetGroup()
        self.qset.beginGroup(shortName)

        self.splitter = eval(str(self.qset.readEntry('/geometry/splitter', '[400, 200]')[0]))
        self.width = self.qset.readNumEntry('/geometry/width', 500)[0]
        self.height = self.qset.readNumEntry('/geometry/height', 600)[0]
        self.quitOnNoChanges = str(self.qset.readEntry('quitOnNoChanges', 'False')[0]) == 'True'
        self.signoff = unicode(self.qset.readEntry('signoff', '')[0])
        self.showUnknown = str(self.qset.readEntry('showUnknown', 'False')[0]) == 'True'
        self._gitExcludeFile = unicode(self.qset.readEntry('gitExcludeFile', '${GIT_DIR}/info/exclude')[0])
        self._gitExcludeDir = unicode(self.qset.readEntry('gitExcludeDir', '.gitignore')[0])

        self.updateGui()

    def updateGui(self):
        self.quitBox.setChecked(self.quitOnNoChanges)
        self.signoffBox.setText(self.signoff)
        self.exclFE.setText(self._gitExcludeFile)
        self.exclDE.setText(self._gitExcludeDir)
        
    def writeSettings(self):
        self.qset.setPath(shortName, shortName)
        self.qset.resetGroup()
        self.qset.beginGroup(shortName)

        self.qset.writeEntry('/geometry/splitter', repr(self.splitter))
        self.qset.writeEntry('/geometry/width', self.width)
        self.qset.writeEntry('/geometry/height', self.height)
        self.qset.writeEntry('quitOnNoChanges', str(self.quitOnNoChanges))
        self.qset.writeEntry('signoff', self.signoff)
        self.qset.writeEntry('showUnknown', str(self.showUnknown))
        self.qset.writeEntry('gitExcludeFile', self._gitExcludeFile)
        self.qset.writeEntry('gitExcludeDir', self._gitExcludeDir)

        # Flush the written entries to disk
        del self.qset
        self.qset = qt.QSettings()
        
    def paintEvent(self, e):
        qt.QDialog.paintEvent(self, e)
    
    def showSettings(self):
        result = False
        if self.exec_loop() == qt.QDialog.Accepted:
            result = True
            self.quitOnNoChanges = self.quitBox.isChecked()
            self.signoff = unicode(self.signoffBox.text())
            self._gitExcludeFile = unicode(self.exclFE.text())
            self._gitExcludeDir = unicode(self.exclDE.text())

        self.updateGui()
        return result

    def gitExcludeFile(self):
        def replace(m):
            if os.environ.has_key(m.group(1)):
                return os.environ[m.group(1)]
            else:
                return ''

        if self._gitExcludeFile == '':
            return None
        else:
            return excludeFileRE.sub(replace, self._gitExcludeFile)

    def gitExcludeDir(self):
        if self._gitExcludeDir == '':
            return None
        else:
            return self._gitExcludeDir

excludeFileRE = re.compile(r'\$\{([^}]*)\}')
